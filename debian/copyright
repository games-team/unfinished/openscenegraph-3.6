Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OpenSceneGraph
Upstream-Contact: Robert Osfield <robert.osfield@gmail.com>
Source: http://www.openscenegraph.org

Files: *
Copyright: 2002-2014, Robert Osfield
License: OSGPL
Comment: The OpenSceneGraph Public License is based on the Lesser GNU
 General Public License (LGPL) and includes the wxWidgets additions to
 LGPL which provide a clear statement that distribution with proprietary
 applications is accepted.

Files: src/osgUtil/tristripper/*
       src/osg/dxtctool.*
       src/osgPlugins/zip/*
       src/osgPlugins/txp/*
Copyright: 2002, Tanguy Fautré
           2009, Neil Hughes
           2002, Boris Bralo
License: zlib

Files: src/osgPlugins/RestHttpDevice/*
Copyright: 2003-2011 Christopher M. Kohlhoff
License: BSL-1.0

Files: src/osg/GLStaticLibrary.h
       src/osg/StateAttribute.cpp
       src/osg/GLStaticLibrary.cpp
       src/osg/Program.cpp
       src/osg/Uniform.cpp
       src/osg/Shader.cpp
       include/osgDB/PluginQuery
       include/osg/Program
       include/osg/Shader
       include/osg/GL2Extensions
       include/osg/Uniform
       applications/osgarchive/osgarchive.cpp
       applications/osgviewer/osgviewer.cpp
       applications/osgfilecache/osgfilecache.cpp
       examples/osgdatabaserevisions/osgdatabaserevisions.cpp
       examples/osgscreencapture/osgscreencapture.cpp
       examples/osgshadergen/osgshadergen.cpp
       examples/osgatomiccounter/osgatomiccounter.cpp
       examples/osguserstats/osguserstats.cpp
Copyright: 2015, Robert Osfield
License: Free_use

Files: src/osgPlugins/xine/*
       src/osgPlugins/cfg/*
       src/osgPlugins/sdl/*
       src/osgPlugins/quicktime/*
       src/osgPlugins/md2/anorms.h
       src/osgPlugins/vrml/*
       src/osgPresentation/*
       include/osgPresentation/*
       applications/present3D/*
       examples/osganimationviewer/*
Copyright: 2015, Robert Osfield
License: GPL-2

Files: src/osgPlugins/cfg/FlexLexer.h
Copyright: 2007, Cedric Pinson
License: BSD-ancient

Files: src/osgPlugins/pov/*
       src/osgPlugins/Inventor/*
       examples/osgsimplegl3/*
Copyright: n/a
License: public-domain

Files: src/osgPlugins/hdr/*
       src/osgPlugins/ply/*
       src/osgPlugins/lwo/*
       src/osgPlugins/3ds/*
       src/osgPlugins/x/*
       include/osgAnimation/Vec3Packed
Copyright: 2004, Mekensleep
           2007, Tobias Wolf
           2005-2007, Stefan Eilemann
           1994, The Board of Trustees of The Leland Stanford
           2007, Tobias Wolf
           2002, Pavel Moloshtan
           2004, Marco Jez
           1996-2008, Jan Eric Kyprianidis
           2002, Ulrich Hertlein
           2001, 2002 Bruno 'Beosil' Heidelberger
License: LGPL

Files: src/osgUtil/PerlinNoise.cpp
       include/osgUtil/PerlinNoise
       src/osgPlugins/osc/*
       src/osg/glu/*
       src/osgViewer/Keystone.cpp
       examples/osgoscdevice/*
       examples/osgfadetext/*
       examples/osghud/*
       examples/osgviewerSDL/*
       examples/osgshape/*
       examples/osgtext3D/*
       examples/osgspacewarp/*
       examples/osgkeystone/*
       examples/osgprerender/*
       examples/osgclip/*
       examples/osgsimulation/*
       examples/osgcubemap/*
       examples/osgdepthpartition/*
       examples/osgcatch/*
       examples/osgcamera/*
       examples/osgcluster/*
       examples/osgpackeddepthstencil/*
       examples/osgfxbrowser/*
       examples/osgmultiviewpaging/*
       examples/osgrobot/*
       examples/osghangglide/*
       examples/osglauncher/*
       examples/osgstereoimage/*
       examples/osgdelaunay/*
       examples/osgshaders/*
       examples/osgbillboard/*
       examples/osgspheresegment/*
       examples/osgpoints/*
       examples/osggameoflife/*
       examples/osgprecipitation/*
       examples/osgparticle/*
       examples/osgmultitexture/*
       examples/osgshadow/terrain_*
       examples/osgpointsprite/*
       examples/osgintersection/*
       examples/osgoccluder/*
       examples/osgthreadedterrain/*
       examples/osgvertexattributes/*
       examples/osgqfont/*
       examples/osgmultitexturecontrol/*
       examples/osgblendequation/*
       examples/osgreflect/*
       examples/osgkeyboardmouse/*
       examples/osgkdtree/*
       examples/osglight/*
       examples/osgsharedarray/*
       examples/osgsidebyside/*
       examples/osglogicop/*
       examples/osgfpdepth/*
       examples/osgmultitouch/*
       examples/osggeometry/*
       examples/osgmultiplerendertargets/*
       examples/osgposter/*
       examples/osgvolume/*
       examples/osgimpostor/*
       examples/osgscalarbar/*
       examples/osgshadercomposition/*
       examples/osggraphicscost/*
       examples/osgmanipulator/*
       examples/osgdistortion/*
       examples/osgimagesequence/*
       examples/osgparametric/*
       examples/osganalysis/*
       examples/osgpagedlod/*
       examples/osgscribe/*
       examples/osgcompositeviewer/*
       examples/osgtexturerectangle/*
       examples/osganimate/*
       examples/osgmultiplemovies/*
       examples/osgmotionblur/*
       examples/osgcallback/*
       examples/osgdrawinstanced/*
       examples/osgoit/*
       examples/osgtexturecompression/*
       examples/osgteapot/*
       examples/osgparticleshader/*
       examples/osglogo/*
       examples/osgsequence/*
       examples/*
       examples/osgocclusionquery/*
       examples/osgphotoalbum/*
       examples/osgviewerCocoa/*
       examples/osggpx/*
       examples/osgtexture2D/*
       examples/osguserdata/*
       examples/osgprerendercubemap/*
       examples/osgmovie/*
       examples/osgcopy/*
       examples/osgtexture3D/*
       examples/osgcomputeshaders/*
       examples/osgviewerFLTK/*
       examples/osglightpoint/*
       examples/osgQtWidgets/*
       examples/osgQtBrowser/*
       examples/osgtext/*
       examples/osgparticleeffects/*
       examples/osgkeyboard/*
       examples/osgvertexprogram/*
       examples/osgautotransform/*
       examples/osgpick/*
       examples/osgslice/*
       examples/osgsimplifier/*
       examples/osgunittests/*
       examples/osgspotlight/*
       examples/osgtexture1D/*
       examples/osgterrain/*
       examples/osgshaderterrain/*
       examples/osgsimpleshaders/*
       examples/osgwindows/*
       examples/osgmemorytest/*
       examples/osgtessellate/*
       examples/osgforest/*
       examples/osgstereomatch/*
Copyright: 2002-2014, Robert Osfield
License: Expat

Files: examples/osgAndroidExampleGLES*/*
Copyright: 2008, The Android Open Source Project
License: Apache-2.0

Files: src/osgPlugins/dae/*
Copyright: 2006, Andrew Lorino and Mike Weiblen
License: SCEA_Shared_Source_License-1.0

License: Apache-2.0
 Found in /usr/share/common-licenses/Apache-2.0

License: BSD-ancient
 Copyright (c) 1993 The Regents of the University of California.
 All rights reserved.
 .
 This code is derived from software contributed to Berkeley by
 Kent Williams and Tom Epperly.
 .
 Redistribution and use in source and binary forms with or without
 modification are permitted provided that: (1) source distributions retain
 this entire copyright notice and comment, and (2) distributions including
 binaries display the following acknowledgement:  ``This product includes
 software developed by the University of California, Berkeley and its
 contributors'' in the documentation or other materials provided with the
 distribution and in all advertising materials mentioning features or use
 of this software.  Neither the name of the University nor the names of
 its contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Free_use
 This application is open source and may be redistributed and/or modified
 freely and without restriction, both in commercial and non commercial applications,
 as long as this copyright notice is maintained.
 .
 This application is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

License: GPL-2
 Found in /usr/share/common-licenses/GPL-2

License: LGPL
 Found in /usr/share/common-licenses/LGPL

License: OSGPL
               OpenSceneGraph Public License, Version 0.0
               ==========================================
 .
 Copyright (C) 2002 Robert Osfield.
 .
 Everyone is permitted to copy and distribute verbatim copies
 of this licence document, but changing it is not allowed.
 .
                      OPENSCENEGRAPH PUBLIC LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 This library is free software; you can redistribute it and/or modify it
 under the terms of the OpenSceneGraph Public License (OSGPL) version 0.0
 or later.
 .
 Notes: the OSGPL is based on the LGPL, with the 4 exceptions laid
 out in the wxWindows section below.  The LGPL is contained in the
 final section of this license.
 .
 .
 -----------------------------------------------------------------------------
 .
               wxWindows Library Licence, Version 3
               ====================================
 .
 Copyright (C) 1998 Julian Smart, Robert Roebling [, ...]
 .
 Everyone is permitted to copy and distribute verbatim copies
 of this licence document, but changing it is not allowed.
 .
                      WXWINDOWS LIBRARY LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public Licence as published by
 the Free Software Foundation; either version 2 of the Licence, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
 General Public Licence for more details.
 .
 You should have received a copy of the GNU Library General Public Licence
 along with this software, usually in a file named COPYING.LIB.  If not,
 write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 EXCEPTION NOTICE
 .
 1. As a special exception, the copyright holders of this library give
 permission for additional uses of the text contained in this release of
 the library as licenced under the wxWindows Library Licence, applying
 either version 3 of the Licence, or (at your option) any later version of
 the Licence as published by the copyright holders of version 3 of the
 Licence document.
 .
 2. The exception is that you may use, copy, link, modify and distribute
 under the user's own terms, binary object code versions of works based
 on the Library.
 .
 3. If you copy code from files distributed under the terms of the GNU
 General Public Licence or the GNU Library General Public Licence into a
 copy of this library, as this licence permits, the exception does not
 apply to the code that you add in this way.  To avoid misleading anyone as
 to the status of such modified files, you must delete this exception
 notice from such code and/or adjust the licensing conditions notice
 accordingly.
 .
 4. If you write modifications of your own for this library, it is your
 choice whether to permit this exception to apply to your modifications.
 If you do not wish that, you must delete the exception notice from such
 code and/or adjust the licensing conditions notice accordingly.
 .
 See also /usr/share/common-licenses/LGPL-2.1

License: SCEA_Shared_Source_License-1.0
 SCEA SHARED SOURCE LICENSE 1.0
 .
 TERMS AND CONDITIONS:
 .
 1. DEFINITIONS:
 .
 "SOFTWARE" SHALL MEAN THE SOFTWARE AND RELATED DOCUMENTATION, WHETHER IN SOURCE OR OBJECT FORM, MADE AVAILABLE UNDER THIS SCEA SHARED SOURCE
 LICENSE ("LICENSE"), THAT IS INDICATED BY A COPYRIGHT NOTICE FILE INCLUDED IN THE SOURCE FILES OR ATTACHED OR ACCOMPANYING THE SOURCE FILES.
 .
 "LICENSOR" SHALL MEAN SONY COMPUTER ENTERTAINMENT AMERICA, INC. (HEREIN "SCEA")
 .
 "OBJECT CODE" OR "OBJECT FORM" SHALL MEAN ANY FORM THAT RESULTS FROM TRANSLATION OR TRANSFORMATION OF SOURCE CODE, INCLUDING BUT NOT LIMITED
 TO COMPILED OBJECT CODE OR CONVERSIONS TO OTHER FORMS INTENDED FOR MACHINE EXECUTION.
 .
 "SOURCE CODE" OR "SOURCE FORM" SHALL HAVE THE PLAIN MEANING GENERALLY ACCEPTED IN THE SOFTWARE INDUSTRY, INCLUDING BUT NOT LIMITED TO SOFTWARE
 SOURCE CODE, DOCUMENTATION SOURCE, HEADER AND CONFIGURATION FILES.
 .
 "YOU" OR "YOUR" SHALL MEAN YOU AS AN INDIVIDUAL OR AS A COMPANY, OR WHICHEVER FORM UNDER WHICH YOU ARE EXERCISING RIGHTS UNDER THIS LICENSE.
 .
 2. LICENSE GRANT.
 .
 LICENSOR HEREBY GRANTS TO YOU, FREE OF CHARGE SUBJECT TO THE TERMS AND CONDITIONS OF THIS LICENSE, AN IRREVOCABLE, NON-EXCLUSIVE, WORLDWIDE,
 PERPETUAL, AND ROYALTY-FREE LICENSE TO USE, MODIFY, REPRODUCE, DISTRIBUTE, PUBLICLY PERFORM OR DISPLAY THE SOFTWARE IN OBJECT OR SOURCE FORM  .
 .
 3. NO RIGHT TO FILE FOR PATENT.
 .
 IN EXCHANGE FOR THE RIGHTS THAT ARE GRANTED TO YOU FREE OF CHARGE UNDER THIS LICENSE, YOU AGREE THAT YOU WILL NOT FILE FOR ANY PATENT
 APPLICATION, SEEK COPYRIGHT PROTECTION OR TAKE ANY OTHER ACTION THAT MIGHT OTHERWISE IMPAIR THE OWNERSHIP RIGHTS IN AND TO THE SOFTWARE THAT
 MAY BELONG TO SCEA OR ANY OF THE OTHER CONTRIBUTORS/AUTHORS OF THE SOFTWARE.
 .
 4. CONTRIBUTIONS.
 .
 SCEA WELCOMES CONTRIBUTIONS IN FORM OF MODIFICATIONS, OPTIMIZATIONS, TOOLS OR DOCUMENTATION DESIGNED TO IMPROVE OR EXPAND THE PERFORMANCE AND
 SCOPE OF THE SOFTWARE (COLLECTIVELY "CONTRIBUTIONS"). PER THE TERMS OF THIS LICENSE YOU ARE FREE TO MODIFY THE SOFTWARE AND THOSE
 MODIFICATIONS WOULD BELONG TO YOU. YOU MAY HOWEVER WISH TO DONATE YOUR CONTRIBUTIONS TO SCEA FOR CONSIDERATION FOR INCLUSION INTO THE
 SOFTWARE. FOR THE AVOIDANCE OF DOUBT, IF YOU ELECT TO SEND YOUR CONTRIBUTIONS TO SCEA, YOU ARE DOING SO VOLUNTARILY AND ARE GIVING THE
 CONTRIBUTIONS TO SCEA AND ITS PARENT COMPANY SONY COMPUTER ENTERTAINMENT, INC., FREE OF CHARGE, TO USE, MODIFY OR DISTRIBUTE IN ANY FORM OR IN
 ANY MANNER. SCEA ACKNOWLEDGES THAT IF YOU MAKE A DONATION OF YOUR CONTRIBUTIONS TO SCEA, SUCH CONTRIBUTIONS SHALL NOT EXCLUSIVELY BELONG TO
 SCEA OR ITS PARENT COMPANY AND SUCH DONATION SHALL NOT BE TO YOUR EXCLUSION. SCEA, IN ITS SOLE DISCRETION, SHALL DETERMINE WHETHER OR NOT TO
 INCLUDE YOUR DONATED CONTRIBUTIONS INTO THE SOFTWARE, IN WHOLE, IN PART, OR AS MODIFIED BY SCEA. SHOULD SCEA ELECT TO INCLUDE ANY SUCH
 CONTRIBUTIONS INTO THE SOFTWARE, IT SHALL DO SO AT ITS OWN RISK AND MAY ELECT TO GIVE CREDIT OR SPECIAL THANKS TO ANY SUCH CONTRIBUTORS IN THE
 ATTACHED COPYRIGHT NOTICE. HOWEVER, IF ANY OF YOUR CONTRIBUTIONS ARE INCLUDED INTO THE SOFTWARE, THEY WILL BECOME PART OF THE SOFTWARE AND
 WILL BE DISTRIBUTED UNDER THE TERMS AND CONDITIONS OF THIS LICENSE. FURTHER, IF YOUR DONATED CONTRIBUTIONS ARE INTEGRATED INTO THE SOFTWARE
 THEN SONY COMPUTER ENTERTAINMENT, INC. SHALL BECOME THE COPYRIGHT OWNER OF THE SOFTWARE NOW CONTAINING YOUR CONTRIBUTIONS AND SCEA WOULD BE
 THE LICENSOR.
 .
 5. REDISTRIBUTION IN SOURCE FORM
 .
 YOU MAY REDISTRIBUTE COPIES OF THE SOFTWARE, MODIFICATIONS OR DERIVATIVES THEREOF IN SOURCE CODE FORM, PROVIDED THAT YOU:
 .
 A. INCLUDE A COPY OF THIS LICENSE AND ANY COPYRIGHT NOTICES WITH SOURCE
 .
 B. IDENTIFY MODIFICATIONS IF ANY WERE MADE TO THE SOFTWARE
 .
 C. INCLUDE A COPY OF ALL DOCUMENTATION ACCOMPANYING THE SOFTWARE AND MODIFICATIONS MADE BY YOU
 .
 6. REDISTRIBUTION IN OBJECT FORM
 .
 IF YOU REDISTRIBUTE COPIES OF THE SOFTWARE, MODIFICATIONS OR DERIVATIVES THEREOF IN OBJECT FORM ONLY (AS INCORPORATED INTO FINISHED GOODS,
 I.E. END USER APPLICATIONS) THEN YOU WILL NOT HAVE A DUTY TO INCLUDE ANY COPIES OF THE CODE, THIS LICENSE, COPYRIGHT NOTICES, OTHER
 ATTRIBUTIONS OR DOCUMENTATION.
 .
 7. NO WARRANTY
 .
 THE SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING,
 WITHOUT LIMITATION, ANY WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. YOU ARE SOLELY
 RESPONSIBLE FOR DETERMINING THE APPROPRIATENESS OF USING, MODIFYING OR REDISTRIBUTING THE SOFTWARE AND ASSUME ANY RISKS ASSOCIATED WITH YOUR
 EXERCISE OF PERMISSIONS UNDER THIS LICENSE.
 .
 8. LIMITATION OF LIABILITY
 .
 UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY WILL EITHER PARTY BE LIABLE TO THE OTHER PARTY OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT,
 CONSEQUENTIAL, SPECIAL, INCIDENTAL, OR EXEMPLARY DAMAGES WITH RESPECT TO ANY INJURY, LOSS, OR DAMAGE, ARISING UNDER OR IN CONNECTION WITH THIS
 LETTER AGREEMENT, WHETHER FORESEEABLE OR UNFORESEEABLE, EVEN IF SUCH PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH INJURY, LOSS, OR
 DAMAGE. THE LIMITATIONS OF LIABILITY SET FORTH IN THIS SECTION SHALL APPLY TO THE FULLEST EXTENT PERMISSIBLE AT LAW OR ANY GOVERMENTAL
 REGULATIONS.
 .
 9. GOVERNING LAW AND CONSENT TO JURISDICTION
 .
 THIS AGREEMENT SHALL BE GOVERNED BY AND INTERPRETED IN ACCORDANCE WITH THE LAWS OF THE STATE OF CALIFORNIA, EXCLUDING THAT BODY OF LAW RELATED
 TO CHOICE OF LAWS, AND OF THE UNITED STATES OF AMERICA. ANY ACTION OR PROCEEDING BROUGHT TO ENFORCE THE TERMS OF THIS AGREEMENT OR TO
 ADJUDICATE ANY DISPUTE ARISING HEREUNDER SHALL BE BROUGHT IN THE SUPERIOR COURT OF THE COUNTY OF SAN MATEO, STATE OF CALIFORNIA OR THE UNITED
 STATES DISTRICT COURT FOR THE NORTHERN DISTRICT OF CALIFORNIA. EACH OF THE PARTIES HEREBY SUBMITS ITSELF TO THE EXCLUSIVE JURISDICTION AND
 VENUE OF SUCH COURTS FOR PURPOSES OF ANY SUCH ACTION. IN ADDITION, EACH PARTY HEREBY WAIVES THE RIGHT TO A JURY TRIAL IN ANY ACTION OR
 PROCEEDING RELATED TO THIS AGREEMENT.
 .
 10. COPYRIGHT NOTICE FOR REDISTRIBUTION OF SOURCE CODE
 .
 COPYRIGHT 2005 SONY COMPUTER ENTERTAINMENT INC.
 .
 LICENSED UNDER THE SCEA SHARED SOURCE LICENSE, VERSION 1.0 (THE "LICENSE"); YOU MAY NOT USE THIS FILE EXCEPT IN COMPLIANCE WITH THE LICENSE.
 YOU MAY OBTAIN A COPY OF THE LICENSE AT:
 .
 HTTP://RESEARCH.SCEA.COM/SCEA_SHARED_SOURCE_LICENSE.HTML
 .
 UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED. SEE THE LICENSE FOR THE SPECIFIC LANGUAGE GOVERNING PERMISSIONS AND
 LIMITATIONS UNDER THE LICENSE.

License: public-domain
 The authors allow any kind of use, modification or distribution of
 the code.

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
